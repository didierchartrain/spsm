import React from 'react';
import { render } from 'react-dom';
import App from './components/App';
const data = window.__DATA__;

render(<App data={data} />, document.getElementById('app'));

module.hot.accept();
