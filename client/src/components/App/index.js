import React, { Component } from 'react';
import style from './style.css';
import PropTypes from 'prop-types';

export default class App extends Component {
  static propTypes = {
    data: PropTypes.object
  };

  render() {
    return <div className={style.app}>{this.props.data.name}</div>;
  }
}
